import java.util.*;
import java.io.*;

/**
 * Created by Mo on 13.12.2016.
 */
public class ChemicalElementHillOperator{

    public ChemicalElementHillOperator() {
    }

    public int compare(ChemicalElement o1, ChemicalElement o2) {
        if(o1.getSymbol() != o2.getSymbol()){
            if(o1.getSymbol() == "C")
                return 1;
            else if(o2.getSymbol() == "C")
                return -1;
            else{
                if(o1.getSymbol() == "H")
                    return 1;
                else if(o2.getSymbol() == "H")
                    return -1;
                else{
                    if(o1.getSymbol().compareTo(o2.getSymbol()) == 1)
                        return 1;
                    else
                        return -1;
                }
            }
        }
        else {
            return 0;
        }
    }
}
