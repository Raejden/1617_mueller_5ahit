import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mo on 13.12.2016.
 */
public class PeriodicTable {
    private int period;
    private int group;

    private Map<Pair<Integer,Integer>, ChemicalElement> table = new HashMap<>();

    public int getPeriod() {
        return period;
    }

    public int getGroup() {
        return group;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public Map<Pair<Integer, Integer>, ChemicalElement> getTable() {
        return table;
    }

    private PeriodicTable(Map<Pair<Integer, Integer>, ChemicalElement> table) {
        this.table = table;
        String line = "";
        String csvSplitBy = ",";
        String line2 = "";
        String csvSplitBy2 = ";";
        FileReader fr;
        BufferedReader br = null;
        FileReader fr2;
        BufferedReader br2 = null;

        try {
            fr = new FileReader("periodensystem.csv");
            br = new BufferedReader(fr);
            fr2 = new FileReader("electronegativity.csv");
            br2 = new BufferedReader(fr2);

            while((line = br.readLine()) != null || (line2 = br2.readLine()) != null){
                String[] element = line.split(csvSplitBy);
                String[] electroNeg = line2.split(csvSplitBy2);
                if (element[2] == electroNeg[0]) {
                    ChemicalElement chemElem = new ChemicalElement(element[2], element[1], Integer.parseInt(element[0]), Integer.parseInt(electroNeg[1]));
                    Pair position = new Pair(Integer.parseInt(element[3]), Integer.parseInt(element[4]));
                    table.put(position, chemElem);
                } else {
                    ChemicalElement chemElem = new ChemicalElement(element[2], element[1], Integer.parseInt(element[0]), 0);
                    Pair position = new Pair(Integer.parseInt(element[3]), Integer.parseInt(element[4]));
                    table.put(position, chemElem);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if(br != null){
                try{
                    br.close();
                } catch(IOException e){
                    e.printStackTrace();
                }
                if(br2 != null){
                    try{
                        br2.close();
                    } catch(IOException e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    ChemicalElement getElement(Pair p){
        return table.get(p);
    }

    Pair getPosition(ChemicalElement element){
        Pair ret = null;
        for(Pair p : table.keySet()){
            if(table.get(p) == element)
                ret = p;
        }
        return ret;
    }
}
