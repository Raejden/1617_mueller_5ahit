import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Mo on 13.12.2016.
 */
public class ChemicalElement implements Comparable<ChemicalElement>, Comparator<ChemicalElement>
{
    final private String name;
    final private String symbol;
    final private int num;

    final private int electroNegativity;

    private static int alkali[] = {3, 11, 19, 37, 55, 87};

    private ArrayList<Integer> trans = new ArrayList<>();

    public ChemicalElement(String name, String symbol, int num, int electroNegativity) {
        this.name = name;
        this.symbol = symbol;
        this.num = num;
        this.electroNegativity = electroNegativity;

        for(int i = 21; i < 32; i++)
            trans.add(i);
        for(int i = 39; i < 49; i++)
            trans.add(i);
        for(int i = 72; i < 81; i++)
            trans.add(i);
        for(int i = 104; i < 113; i++)
            trans.add(i);
    }

    public int getElectroNegativity() {
        return electroNegativity;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getNum() {
        return num;
    }

    boolean isAlkaliMetal(){
        int i = 0;
        while(i < 6){
            if(this.num == alkali[i])
                return true;
            i++;
        }
        return false;
    }

    ////////?Da best?/////////////
    boolean isMetal() {
        switch (num) {
            case 13: return true;
            case 49: return true;
            case 50: return true;
            case 81: return true;
            case 82: return true;
            case 83: return true;
            case 113: return true;
            case 114: return true;
            case 115: return true;
            case 116: return true;
        }
        return false;
    }
    /////////?Da best?/////////////

    boolean isTransitionalMetal(){
        if(trans.contains(this.num))
            return true;
        else
            return false;
    }

    @Override
    public int compareTo(ChemicalElement o) {
        if(this.electroNegativity < o.electroNegativity)
            return -1;
        else if(this.electroNegativity > o.electroNegativity)
            return 1;
        else
            return 0;
    }

    @Override
    public int compare(ChemicalElement o1, ChemicalElement o2) {
        if(o1.electroNegativity < o2.electroNegativity)
            return -1;
        else if(o1.electroNegativity > o2.electroNegativity)
            return 1;
        else
            return 0;
    }
}
