/**
 * Created by Mo on 13.12.2016.
 */
public class Pair<P,G> {

    private final P period;
    private final G group;

    public Pair(P period, G group) {
        this.period = period;
        this.group = group;
    }

    public P getPeriod() { return period; }
    public G getGroup() { return group; }

    @Override
    public int hashCode() { return period.hashCode() ^ group.hashCode(); }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) return false;
        Pair pairo = (Pair) o;
        return this.period.equals(pairo.getPeriod()) &&
                this.group.equals(pairo.getGroup());
    }

}
