package command;/*
 * Command Processor Test - demonstrates COMMAND PROCESSOR pattern.
 */

import org.junit.BeforeClass;
import org.junit.Test;
import util.LogUtils;
import util.MiscUtils;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
 * Command Processor Test - demonstrates COMMAND PROCESSOR pattern.
 */
public class InputCommandProcessorTest {

    /**
     * logger
     */
    protected static final Logger LOGGER = Logger.getLogger(InputCommandProcessorTest.class.getName());

    /**
     * Log-level for this test
     */
    private static final Level LOG_LEVEL = Level.INFO;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LogUtils.setConsoleHandlerLogLevel(LOG_LEVEL);
        LogUtils.setLogLevel(LOG_LEVEL, InputCommandProcessorTest.class, InputCommandProcessor.class,
                AppendTextCommand.class, DeleteTextCommand.class);
    }

    @Test
    public void testCommandProcessor() {

        // hint: set the log-level above to FINE to watch COMMAND PROCESSOR working.

        LOGGER.info("Test Commmand Processor ...");
        long startTimeNanos = System.nanoTime();

        TextComponent textComponent = new TextComponent();

        InputCommandProcessor inputCommandProcessor = new InputCommandProcessor();

        List<InputCommand> commandList = Arrays.asList(new InputCommand[]{
                new AppendTextCommand(textComponent, "The"), new AppendTextCommand(textComponent, "quick"),
                new DeleteTextCommand(textComponent, 1), new DeleteTextCommand(textComponent, 1),
                new DeleteTextCommand(textComponent, 1), new DeleteTextCommand(textComponent, 1),
                new DeleteTextCommand(textComponent, 1), new AppendTextCommand(textComponent, " "),
                new AppendTextCommand(textComponent, "quick"), new AppendTextCommand(textComponent, " "),
                new AppendTextCommand(textComponent, "brown"), new AppendTextCommand(textComponent, " "),
                new AppendTextCommand(textComponent, "d"), new DeleteTextCommand(textComponent, 1),
                new AppendTextCommand(textComponent, "f"), new AppendTextCommand(textComponent, "o"),
                new AppendTextCommand(textComponent, "x"), new AppendTextCommand(textComponent, " "),
                new AppendTextCommand(textComponent, "jumped over the lazy frog"),
                new DeleteTextCommand(textComponent, 4), new AppendTextCommand(textComponent, "duck"),
                new DeleteTextCommand(textComponent, 1), new DeleteTextCommand(textComponent, 1),
                new DeleteTextCommand(textComponent, 1), new AppendTextCommand(textComponent, "og."),});

        for (InputCommand ic : commandList) {
            inputCommandProcessor.execute(ic);
        }

        assertEquals("The quick brown fox jumped over the lazy dog.", textComponent.toString());

        boolean res = false;
        do {
            res = inputCommandProcessor.undo();
        } while (res);



        assertEquals("", textComponent.toString());

        do {
            res = inputCommandProcessor.redo();
        } while (res);

        assertEquals("The quick brown fox jumped over the lazy dog.", textComponent.toString());

        LOGGER.info("Test Commmand Processor successful! Elapsed time: "
                + MiscUtils.formatNanosAsSeconds(System.nanoTime() - startTimeNanos) + " s");
    }

}
