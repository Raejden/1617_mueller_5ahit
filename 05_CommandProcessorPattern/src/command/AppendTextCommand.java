package command;

import java.util.logging.Logger;

/**
 * Text Command - a concrete COMMAND.
 */
public class AppendTextCommand extends InputCommand {

    /**
     * logger
     */
    protected static final Logger LOGGER = Logger.getLogger(AppendTextCommand.class.getName());

    /**
     * characters to be appended by this command
     */
    private final String text;

    /**
     * Creates new append text command for the given text.
     * @param receiver instance to execute the action
     * @param text string to be appended
    */
    public AppendTextCommand(TextComponent receiver, String text) {
        super(receiver);
        LOGGER.fine("New " + this.getClass().getSimpleName() + " created to append '" + text + "'.");
        this.text = text;
    }

    @Override
    public void execute() {
        LOGGER.fine("Executing " + this.getClass().getSimpleName() + " to append '" + text + "'.");
        receiver.append(text);
    }

    @Override
    public void executeUndo() {
        LOGGER.fine("Undoing " + this.getClass().getSimpleName() + " to remove '" + text + "'.");
        receiver.setLength(receiver.length() - text.length());
    }

}
