package command;

import java.util.logging.Logger;

/**
 * Delete Text Command - a concrete COMMAND.
 */
public class DeleteTextCommand extends InputCommand {

    /**
     * logger
     */
    protected static final Logger LOGGER = Logger.getLogger(DeleteTextCommand.class.getName());

    /**
     * Number of characters to be deleted
     */
    private final int numberOfCharactersToBeDeleted;

    /**
     * for undoing we store the characters that were deleted.
     */
    private String removedCharacters = null;

    /**
     * Creates new deletion command.
     * @param receiver command destination
     * @param size number of characters to be deleted.
     */
    public DeleteTextCommand(TextComponent receiver, int size) {
        super(receiver);
        LOGGER.fine("New " + this.getClass().getSimpleName() + " created to delete " + size + " character(s).");
        this.numberOfCharactersToBeDeleted = size;
    }

    @Override
    public void execute() {
        LOGGER.fine("Executing " + this.getClass().getSimpleName() + " to delete " + this.numberOfCharactersToBeDeleted
                + " character(s).");
        int newLength = receiver.length() - this.numberOfCharactersToBeDeleted;
        removedCharacters = receiver.substring(newLength);
        receiver.setLength(newLength);
    }

    @Override
    public void executeUndo() {
        LOGGER.fine("Undoing " + this.getClass().getSimpleName() + " to undelete " + this.numberOfCharactersToBeDeleted
                + " character(s).");
        receiver.append(removedCharacters);
    }

}
