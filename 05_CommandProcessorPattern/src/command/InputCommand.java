package command;

/**
 * Input Command - to be extended by concrete COMMANDs.
 */
public abstract class InputCommand {

    /**
     * The component to delegate to
     */
    protected final TextComponent receiver;

    /**
     * Creates new input command
     * @param receiver the component we delegate to for executing the action
     */
    public InputCommand(TextComponent receiver) {
        this.receiver = receiver;
    }

    /**
     * Executes the command
     */
    public abstract void execute();

    /**
     * Applies the reverse command
     */
    public abstract void executeUndo();

}
