package command;

import java.util.logging.Logger;

/**
 * Text Component - the RECEIVER in this COMMAND example.
 */
public class TextComponent {

    /**
     * logger
     */
    protected static final Logger LOGGER = Logger.getLogger(TextComponent.class.getName());

    /**
     * To keep example simple we just wrap a string builder
     */
    private final StringBuilder stringBuilder = new StringBuilder();

    /**
     * Returns current size of text, number of characters
     * @return length
     */
    public int length() {
        return stringBuilder.length();
    }

    /**
     * Sets the new length of the internal buffer
     * @param newLength length to be set
     */
    public void setLength(int newLength) {
        LOGGER.fine(this.getClass().getSimpleName() + ".setLength(" + newLength + ") called.");
        stringBuilder.setLength(newLength);
    }

    /**
     * appends the given string to the internal buffer
     * @param str string to be appended
     * @return this instance
     */
    public TextComponent append(String str) {
        LOGGER.fine(this.getClass().getSimpleName() + ".append('" + str + "') called.");
        stringBuilder.append(str);
        return this;
    }

    /**
     * Returns a string containing the string copied beginning with the given position
     * @param start first character to be copied
     * @return string
     */
    public String substring(int start) {
        return stringBuilder.substring(start);
    }

    @Override
    public String toString() {
        return stringBuilder.toString();
    }

}
