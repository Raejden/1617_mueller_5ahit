/*
 * Log Utilities
 */
package util;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Some utility methods related to java logging.
 */
public final class LogUtils {

    /**
     * Utility class
     */
    private LogUtils() {
        // no instances
    }
    
    /**
     * Adjust the console handler's log level, especially useful in some IDEs.
     * @param level required level
     */
    public static void setConsoleHandlerLogLevel(Level level) {
        Logger rootLogger = Logger.getLogger("");

        // find console handler, if any
        for (Handler handler : rootLogger.getHandlers()) {
            if (handler instanceof ConsoleHandler) {
                handler.setLevel(level);
                return;
            }
        }

        // ok, we need a new console handler
        ConsoleHandler consoleHandler = new ConsoleHandler();
        rootLogger.addHandler(consoleHandler);
        consoleHandler.setLevel(level);
    }

    /**
     * Sets the log level related to multiple loggers.
     * @param level new level to set
     * @param loggers one or more loggers
     */
    public static void setLogLevel(Level level, Logger... loggers) {
        for (Logger logger : loggers) {
            logger.setLevel(level);
        }
    }

    /**
     * Sets the log level related to multiple classes assuming the class name as the related logger's name.
     * @param level new level to set
     * @param classes one or more classes
     */
    public static void setLogLevel(Level level, Class<?>... classes) {
        for (Class<?> cls : classes) {
            Logger.getLogger(cls.getName()).setLevel(level);
        }
    }

}
