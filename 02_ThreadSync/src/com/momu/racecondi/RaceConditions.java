package com.momu.racecondi;

/**
 * Created by Mo on 11.10.2016.
 */
public class RaceConditions
{
    static int x = 0, y = 0;
    static int a =0, b = 0;

    public static void main(String[] args) throws InterruptedException {

        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                a = 1;
                System.out.println("a set");
                x = b;
                System.out.println("x set");
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                b =     1;
                System.out.println("b set");
                y = a;
                System.out.println("y set");
            }
        });
        threadA.start();
        threadB.start();
        threadA.join();
        threadB.join();
        System.out.println("(" + x + ", " + y + ")");
    }
}
