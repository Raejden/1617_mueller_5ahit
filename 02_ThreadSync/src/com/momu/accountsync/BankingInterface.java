package com.momu.accountsync;

/**
 * Created by Mo on 11.10.2016.
 */
public class BankingInterface {
    public static void main(String[] args) {

        ThreadSafeBankAccount acc1 = new ThreadSafeBankAccount(1, 100);
        ThreadSafeBankAccount acc2 = new ThreadSafeBankAccount(2, 100);


        //Teller teller = new Teller(acc1, acc2, 50);

        Runnable teller = new Teller(acc1, acc2, 50);

        Thread thread1 = new Thread(teller);

        thread1.start();

        try {
            thread1.join();
        } catch (InterruptedException e) {
        }

        System.out.println(acc1.getBalance());
        System.out.println(acc2.getBalance());
    }
}
