package com.momu.accountsync;

/**
 * Created by Mo on 11.10.2016.
 */
public class Teller implements Runnable {

    private ThreadSafeBankAccount accountFrom;
    private ThreadSafeBankAccount accountTo;
    private int amount;

    public Teller(ThreadSafeBankAccount accountFrom, ThreadSafeBankAccount accountTo, int amount)
    {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amount = amount;
    }

    @Override
    public void run() {
        transfer(this.accountFrom, this.accountTo, this.amount);
    }

    public void transfer(ThreadSafeBankAccount accountFrom, ThreadSafeBankAccount accountTo, int amount)
    {
        accountFrom.withdraw(amount);
        accountTo.deposit(amount);
    }
}
