package com.momu.accountsync;

/**
 * Created by Mo on 11.10.2016.
 */
public class ThreadSafeBankAccount {

    private int accountNumber;
    private int balance;

    private Object accountLock = new Object();

    public ThreadSafeBankAccount(int accountNumber, int balance)
    {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public int getBalance()
    {
        synchronized (accountLock){
            return balance;
        }
    }

    public int getAccountNumber()
    {
        return accountNumber;
    }

    public void deposit(int amount)
    {
        synchronized (accountLock) {
            this.balance += amount;
        }
    }

    public void withdraw(int amount)
    {
        synchronized (accountLock) {
            this.balance -= amount;
        }
    }


}
