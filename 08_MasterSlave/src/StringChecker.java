import java.lang.reflect.Array;

/**
 * Created by Mo on 31.01.2017.
 */
public class StringChecker
{
    public StringChecker(){

    }

    static boolean checkPalindrome(String text){
        int n = 0;
        int max = text.length() - 1;
        char[] letters = new char[20];
        boolean chk = true;

        while(n <= max){
            letters[max-n] = text.charAt(n);
            n++;
        }

        n=0;
        while(chk != false) {
            if (letters[n] != text.charAt(n))
                chk = false;
        }

        return chk;
    }
}
