import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringCheckerTest {
    String text1;
    String text2;
    String text3;
    String text4;
    String text5;
    StringChecker Chk;

    @Before
    public void setUp() throws Exception{
        text1 = "otto";
        text2 = "NotTrue";
        text3 = "anxna";
        text4 = "Otto";
        text5 = "ottO";
        Chk = new StringChecker();

    }

    @Test
    public void testCheckPalindrome() throws Exception {
        assertTrue(Chk.checkPalindrome(text1));
        assertFalse(Chk.checkPalindrome(text2));
        assertTrue(Chk.checkPalindrome(text3));
        assertTrue(Chk.checkPalindrome(text4));
        assertTrue(Chk.checkPalindrome(text5);
    }
}
