/**
 * Created by Mo on 11.10.2016.
 */
import java.util.Random;

public class Printer implements Runnable {
    String name="";

    public Printer(String s){
        name=s;
    }

    @Override
    public void run() {
        int u=0;
        try {
            while(!Thread.interrupted()){
                Random random= new Random();
                int time = 1+random.nextInt(9);
                System.out.println(name);
                System.out.println("Sleepingtime:" + time +" ");
                System.out.println("Counter:" + u + "\n");
                //Thread.sleep(time * 100);
                Thread.sleep(1);
                u++;
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
