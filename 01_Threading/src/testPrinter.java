/**
 * Created by Mo on 11.10.2016.
 */
public class testPrinter {
    public static void main(String[] arg) throws InterruptedException {
        /*
        ue1-4
        Runnable runnable = new Printer("T1");
        Runnable runnable2 = new Printer("T2");
        Thread threads = new Thread(runnable);
        Thread thread = new Thread(runnable2);
        threads.start();
        thread.start();
        threads.join(5000);
        thread.join(3000);
        Thread.sleep(2000);
        threads.interrupt();
        thread.interrupt();*/

        Runnable runnable1 = new Printer("+");
        Runnable runnable2 = new Printer("-");
        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable2);
        thread1.start();
        thread2.start();
        thread1.join(1000);
        thread2.join(1000);
        while (thread1.isInterrupted()){
            System.out.println("*");
        }

    }
}
