import java.util.concurrent.BlockingQueue;

/**
 * Created by Mo on 08.11.2016.
 */
public class Player implements Runnable
{
    protected BlockingQueue queue = null;
    private Value select;

    public Player(BlockingQueue queue, Value select) {
        this.queue = queue;
        this.select = select;
    }

    @Override
    public void run() {
        try{
            queue.put(select);
        }

        catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
