import java.util.concurrent.BlockingQueue;

/**
 * Created by Mo on 08.11.2016.
 */
public class Comparer implements Runnable
{
    protected BlockingQueue queue_p1 = null;
    protected BlockingQueue queue_p2 = null;

    public Comparer(BlockingQueue q1, BlockingQueue q2){
        this.queue_p1 = q1;
        this.queue_p2 = q2;
    }

    @Override
    public void run() {
        try{
            queue_p1.take();
        }

        catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
