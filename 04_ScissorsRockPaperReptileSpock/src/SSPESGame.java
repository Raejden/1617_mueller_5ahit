/**
 * Created by Mo on 08.11.2016.
 */
public class SSPESGame
{
    public String EvaluateResult(Value v1, Value v2){
        if(v1.equals(v2))
            return "Draw!";

        if(v1.equals(Value.SCISSORS) && (v2.equals(Value.PAPER) || v2.equals(Value.REPTILE)))
            return "Player 1 wins!";
        if(v2.equals(Value.SCISSORS) && (v1.equals(Value.PAPER) || v1.equals(Value.REPTILE)))
            return "Player 2 wins!";

        if(v1.equals(Value.PAPER) && (v2.equals(Value.ROCK) || v2.equals(Value.SPOCK)))
            return "Player 1 wins!";
        if(v2.equals(Value.PAPER) && (v1.equals(Value.ROCK) || v1.equals(Value.SPOCK)))
            return "Player 2 wins!";

        if(v1.equals(Value.ROCK) && (v2.equals(Value.SCISSORS) || v2.equals(Value.REPTILE)))
            return "Player 1 wins!";
        if(v2.equals(Value.ROCK) && (v1.equals(Value.SCISSORS) || v1.equals(Value.REPTILE)))
            return "Player 2 wins!";

        if(v1.equals(Value.REPTILE) && (v2.equals(Value.PAPER) || v2.equals(Value.SPOCK)))
            return "Player 1 wins!";
        if(v2.equals(Value.REPTILE) && (v1.equals(Value.PAPER) || v1.equals(Value.SPOCK)))
            return "Player 2 wins!";

        if(v1.equals(Value.SPOCK) && (v2.equals(Value.SCISSORS) || v2.equals(Value.ROCK)))
            return "Player 1 wins!";
        if(v2.equals(Value.SPOCK) && (v1.equals(Value.SCISSORS) || v1.equals(Value.ROCK)))
            return "Player 2 wins!";

        return "Error";
    }
}
