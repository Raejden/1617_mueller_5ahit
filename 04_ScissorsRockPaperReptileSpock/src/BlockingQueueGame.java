import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Mo on 08.11.2016.
 */
public class BlockingQueueGame {
    public static void main(String[] args) throws Exception {
        BlockingQueue queue_p1 = new ArrayBlockingQueue(1);
        BlockingQueue queue_p2 = new ArrayBlockingQueue(1);

        Player player1 = new Player(queue_p1, Value.SCISSORS);
        Player player2 = new Player(queue_p2, Value.PAPER);
        Comparer comp = new Comparer(queue_p1, queue_p2);
    }
}
