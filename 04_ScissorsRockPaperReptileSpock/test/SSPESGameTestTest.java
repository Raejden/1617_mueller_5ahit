import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mo on 08.11.2016.
 */
public class SSPESGameTestTest {

    SSPESGame game = new SSPESGame();

    @Test
    public void testTestEvaluateResult() throws Exception {
        assertEquals(game.EvaluateResult(Value.REPTILE, Value.SCISSORS), "Player 2 wins!");
        assertEquals(game.EvaluateResult(Value.SCISSORS, Value.SCISSORS), "Draw!");
        assertEquals(game.EvaluateResult(Value.SCISSORS, Value.REPTILE), "Player 1 wins!");
    }
}