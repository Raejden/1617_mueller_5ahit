import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Mo on 25.10.2016.
 */
public class BasicThreadPoolExecutorExample {
    public static void main(String[] args)
    {
//Use the executor created by the newCachedThreadPool() method
//only when you have a reasonable number of threads
//or when they have a short duration.

        //declare a 'collection' of Tasks
        ThreadPoolExecutor executor = (ThreadPoolExecutor)
                Executors.newCachedThreadPool();
        for (int i = 0; i <= 5; i++)
        {
            Task task = new Task("Task " + i);
            System.out.println("A new task has been added : " +
                    task.getName());
            executor.execute(task); //Executor executes the current task
        }
        executor.shutdown();        //close executor, executor waits for all threads to finish first
    }
}
