import java.util.concurrent.TimeUnit;

/**
 * Created by Mo on 25.10.2016.
 */
class Task implements Runnable {
    private String name;

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {
        try {
            Long duration = (long) (Math.random() * 10);
            System.out.println("Doing a task during : " + name);
            TimeUnit.SECONDS.sleep(duration); // instead of Thread.sleep(1000)
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
