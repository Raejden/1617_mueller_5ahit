package Multithread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Mo on 07.03.2017.
 */
//source:http://www.baeldung.com/a-guide-to-java-sockets
public class MultiClient {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);    //ip & port of destination server
            out = new PrintWriter(clientSocket.getOutputStream(), true);    //stream to send messages to server
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));  //stream to receive messages from server
        } catch(IOException e){}
    }

    public String sendMessage(String msg) {
        String resp="";
        try {
            out.println(msg);       //send message to server
            resp = in.readLine();   //read server response

        } catch(IOException e){}
        return resp;
    }

    public void stopConnection() {
        try {
            in.close();
            out.close();
            clientSocket.close();
        } catch(IOException e){}
    }
}
