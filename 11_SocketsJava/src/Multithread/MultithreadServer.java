package Multithread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Mo on 07.03.2017.
 */
public class MultithreadServer {
    private ServerSocket serverSocket;

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            while (true) {//stop?
                System.out.println("Waiting for client.");
                Socket s = serverSocket.accept();
                new EchoClientHandler(s).start();
            }
        } catch (Exception e) {
        }
    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (Exception e) {
        }
    }

    private static class EchoClientHandler extends Thread {
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;

        public EchoClientHandler(Socket socket) {
            this.clientSocket = socket;
        }

        public void run() {
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));

                String inputLine;

                System.out.println("Waiting for input.");
                while ((inputLine = in.readLine()) != null) {
                    if (".".equals(inputLine)) {
                        System.out.println("Client disconnecting...");
                        out.println("bye");
                        break;
                    }
                    out.println(inputLine);
                }

                in.close();
                out.close();
                clientSocket.close();
                System.out.println("Client successfully disconnected!");
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        MultithreadServer server = new MultithreadServer();
        server.start(9999);
    }
}
