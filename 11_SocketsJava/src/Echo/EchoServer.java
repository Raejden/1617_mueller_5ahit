package Echo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Mo on 07.03.2017.
 */
public class EchoServer {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void start(int port) throws IOException {
        try {
            serverSocket = new ServerSocket(port);  // initialize server socket
            clientSocket = serverSocket.accept();   // initialize client if one tries to connect
            out = new PrintWriter(clientSocket.getOutputStream(), true);    // stream to send messages to client
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));  //stream to read messages from client (buffered = waits for a whole string to arrive before continuing

            while(true) {
                String msg = in.readLine();    //read message sent by client

                if("stop".equals(msg)) {
                    out.println("stopping server...");
                    stop();
                }

                //react to message sent by client
                out.println(msg);
            }
        } catch (IOException e){
            System.out.println("error");
        }
    }

    public void stop() {
        try {
            in.close();
            out.close();
            clientSocket.close();
            serverSocket.close();
        } catch(IOException e){}
    }

    public static void main(String[] args) {
        try {
            EchoServer server = new EchoServer();
            server.start(9999);
        } catch(IOException e){}
    }
}
