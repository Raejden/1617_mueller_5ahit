package Echo;

import Echo.EchoClient;

import static org.junit.Assert.*;

/**
 * Created by Mo on 07.03.2017.
 */
public class EchoClientTest {

    @org.junit.Test
    public void testSendMessage() throws Exception {
        EchoClient client = new EchoClient();
        client.startConnection("127.0.0.1", 9999);
        String a = "a";
        String b = "b";
        String c = "c";

        String response = client.sendMessage(a);
        assertEquals(a, response);
        System.out.println(response);

        response = client.sendMessage(b);
        assertEquals(b, response);
        System.out.println(response);

        response = client.sendMessage(c);
        assertEquals(c, response);
        System.out.println(response);

        response = client.sendMessage("stop");
        assertEquals("stopping server...", response);
        System.out.println(response);
    }
}