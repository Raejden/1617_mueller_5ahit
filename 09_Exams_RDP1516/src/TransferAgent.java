/**
 * Created by Mo on 07.02.2017.
 */
public class TransferAgent implements Runnable{
    static final int pause = 500;
    static final int transferNumbers = 200;

    private double amount;
    private Account to;
    private Account from;

    int i = 0;

    public TransferAgent(double amount, Account to, Account from) {
        this.amount = amount;
        this.to = to;
        this.from = from;
    }

    @Override
    public void run() {
        while(i < transferNumbers) {
            to.deposit(amount);
            from.withdraw(amount);
            try {
                Thread.sleep(pause);
            } catch(InterruptedException e){

            }
            i++;
        }
    }
}
