/**
 * Created by Mo on 07.02.2017.
 */
public class Account {

    double balance;
    int id;

    private Object accountLock = new Object();

    public Account(double balance, int id)
    {
        this.balance = balance;
        this.id = id;
    }

    void withdraw(double amount){
        synchronized(accountLock) {
            this.balance -= amount;
        }
    }

    void deposit(double amount){
        synchronized(accountLock) {
            this.balance += amount;
        }
    }
}
