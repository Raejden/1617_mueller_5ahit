import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by Mo on 07.02.2017.
 */
public class PlotterTest {

    String f1;
    String f2;
    String f3;
    String f4;
    String f5;
    String f6;
    String f7;
    String f8;
    Plotter plotter = new Plotter();

    @org.junit.Before
    public void setUp() throws Exception {
        f1 = "x + (-5)";        //error
        f2 = "x";               //
        f3 = "-0 + 800 * x ";   //
        f4 = "x^2-4";           //
        f5 = "x*5 - y/2";       //error
        f6 = "x/0";             //error
        f7 = "x+5*10";          //
        f8 = "x^3+666^2";       //
    }

    @org.junit.Test
    public void testEvalFunction() throws Exception {
        assertEquals(0, plotter.evalFunction(f2,0));
        assertEquals(800, plotter.evalFunction(f3,1));
        assertEquals(12,plotter.evalFunction(f4,4));
        assertEquals(0,plotter.evalFunction(f7,(-50)));
        assertEquals(666*666, plotter.evalFunction(f8,0));
    }
}