/**
 * Created by Mo on 07.02.2017.
 */
public class TransferAccountTest {
    public static void main(String[] args) {
        Account acc1 = new Account(10000, 0);
        Account acc2 = new Account(10000, 1);

        Runnable ta = new TransferAgent(200, acc1, acc2);

        Thread t1 = new Thread(ta);

        t1.start();

        try{
            t1.join();
        } catch(InterruptedException e){

        }

        System.out.println("Success!");
    }
}
